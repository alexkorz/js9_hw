//TASK 1

let feature = document.querySelectorAll('.feature');
let feature2 = document.getElementsByClassName('feature');
console.log('feature with query selector', feature)
console.log('feature with getElementsByClassName', feature2)

feature.forEach(el => el.style.textAlign = 'center');

//TASK 2

document.querySelectorAll('h2').forEach(el => el.textContent = 'Awesome feature')

//TASK 3

document.querySelectorAll('.feature-title').forEach(el => el.textContent += '!');